package com.wujq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;

class WorkThrottlerSemaphore {

    // timeout is constant but can be easily changed to be manageable
    public static final long TIMEOUT = 2000;
    // if workTime equals default int value this implies that
    // was not set and random work time between 0 and 2 seconds is used
    private int workTime;
    private int parallelQueriesPerUserLimit;
    private Random random = new Random();
    private Semaphore queriesPermits;
    private Semaphore usersPermits;
    private final Map<String, Semaphore> parallelQueriesPerUserPermits = new ConcurrentHashMap<>();

    public WorkThrottlerSemaphore(int parallelQueriesLimit, int parallelUsersLimit, int parallelQueriesPerUserLimit) {
        this.queriesPermits = new Semaphore(parallelQueriesLimit);
        this.usersPermits = new Semaphore(parallelUsersLimit);
        this.parallelQueriesPerUserLimit = parallelQueriesPerUserLimit;
    }

    // constructor created for tests purposes
    // stable work time can be set for predictable outcomes of tests
    public WorkThrottlerSemaphore(int parallelQueriesLimit, int parallelUsersLimit, int parallelQueriesPerUserLimit, int workTime) {
        this(parallelQueriesLimit, parallelUsersLimit, parallelQueriesPerUserLimit);
        this.workTime = workTime;
    }

    boolean work(String name) {
        List<Semaphore> semaphores = new CopyOnWriteArrayList<>();
        try {
            semaphores = tryToAcquireAllPermits(name);
            // can be replaced by come tuple object with a boolean field to check if successfully acquired
            if (semaphores.contains(queriesPermits)) {
                Thread.sleep(getWorkTime());
                return true;
            } else {
                return false;
            }
        } catch (InterruptedException e) {
            System.out.println(name + " interrupted exception");
            return false;
        } finally {
            // releasing acquired semaphores
            System.out.println("releasing " + semaphores.size() + " semaphores");
            semaphores.forEach(semaphore -> {
                if (semaphore != null) {
                    semaphore.release();
                }
            });
            // removes user entry from map when all queries finish for this username
            if (parallelQueriesPerUserPermits.containsKey(name) && parallelQueriesPerUserPermits.get(name).availablePermits() == parallelQueriesPerUserLimit) {
                parallelQueriesPerUserPermits.remove(name);
            }
        }
    }

    private List<Semaphore> tryToAcquireAllPermits(String name) {
        ExecutorService acquireExecutor = Executors.newSingleThreadExecutor();
        List<Semaphore> semaphores = new ArrayList<>();
        long timer = System.currentTimeMillis();
        acquireExecutor.submit(acquireQueriesPermitRunnable(semaphores, name));
        awaitTerminationAfterShutdown(acquireExecutor);
        System.out.println("semaphores to release size " + semaphores.size() + " I was waiting for: " + (System.currentTimeMillis() - timer));
        return semaphores;
    }

    public void awaitTerminationAfterShutdown(ExecutorService executorService) {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(TIMEOUT, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    // method created for tests purposes
    // stable amount of work time can be set for predictable outcome of tests
    private int getWorkTime() {
        if (this.workTime == 0) {
            return random.nextInt(2000);
        } else {
            return workTime;
        }
    }

    // method firstly acquiring user permits
    // because many requests from one user could quickly get permits
    // for overall number of queries and in this manner starve requests from other users
    private Runnable acquireQueriesPermitRunnable(List<Semaphore> semaphores, String name) {
        return () -> {
            System.out.println("Acquire query permits");
            Long localTimeout = new Long(TIMEOUT);
            try {
                long timeoutHelper = System.currentTimeMillis();
                if (parallelQueriesPerUserPermits.containsKey(name)) {
                    synchronized (parallelQueriesPerUserPermits) {
                        if (parallelQueriesPerUserPermits.containsKey(name)) {
                            localTimeout = localTimeout - (System.currentTimeMillis() - timeoutHelper);
                            System.out.println("Local timeout if: " + localTimeout);
                            if(parallelQueriesPerUserPermits.get(name).tryAcquire(localTimeout, TimeUnit.MILLISECONDS)) {
                                semaphores.add(parallelQueriesPerUserPermits.get(name));
                            }
                            timeoutHelper = System.currentTimeMillis();
                        }
                    }
                } else {
                    synchronized (usersPermits) {
                        localTimeout = localTimeout - (System.currentTimeMillis() - timeoutHelper);
                        System.out.println("Local timeout else: " + localTimeout);
                        if (usersPermits.tryAcquire(localTimeout, TimeUnit.MILLISECONDS)) {
                            semaphores.add(usersPermits);
                            Semaphore workPerUserPermit = new Semaphore(parallelQueriesPerUserLimit);
                            // freshly instantiated semaphore
                            // almost instant acquisition
                            workPerUserPermit.acquire();
                            parallelQueriesPerUserPermits.putIfAbsent(name, workPerUserPermit);
                            semaphores.add(parallelQueriesPerUserPermits.get(name));
                        }
                        timeoutHelper = System.currentTimeMillis();
                    }
                }
                synchronized (queriesPermits) {
                    localTimeout = localTimeout - (System.currentTimeMillis() - timeoutHelper);
                    System.out.println("Local timeout all work stage: " + localTimeout);
                    if (queriesPermits.tryAcquire(localTimeout, TimeUnit.MILLISECONDS)) {
                        semaphores.add(queriesPermits);
                    }
                }
            } catch (InterruptedException e) {
                System.out.println("Acquiring query permits timed out");
            }
            System.out.println("acquiring finished");
        };
    }

}
