package com.wujq;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class WorkThrottleSemaphoreTest {

    private static List<String> sameNames = new ArrayList<>();
    private static List<String> differentNames = new ArrayList<>();

    @BeforeAll
    public static void setUp() {
        // create username lists
        for (int i = 0; i < 10; i++) {
            sameNames.add("User");
            differentNames.add("User" + i);
        }
    }

    @Test
    public void allUserQueriesShouldSuccessfullySubmit() throws ExecutionException, InterruptedException {
        // pool size equal number of users to make all requests simultaneously
        ForkJoinPool forkJoinPool = new ForkJoinPool(10);
        WorkThrottlerSemaphore workThrottlerSemaphore = new WorkThrottlerSemaphore(10, 1, 10, 1000); // MyClass is tested

        // run all requests and get results of execution to boolean list
        List<Boolean> resultList = forkJoinPool.submit(
                () -> sameNames.parallelStream().map(s -> {
                    boolean result = workThrottlerSemaphore.work(s);
                    System.out.println(result);
                    return result;
                }).collect(Collectors.toList())).get();

        System.out.println(resultList);
        assertEquals(10, resultList.size());
        assertTrue(resultList.contains(true));
        assertFalse(resultList.contains(false));
    }

    @Test
    public void oneUserWithManyRequestsShouldNotStarvOtherUsersRequests() throws ExecutionException, InterruptedException {
        int workPerUserLimit = 1;
        int allQueriesLimit = 11;
        int allUsersLimit = 11;
        int workTimeInMillis = 2000;
        // pool size equal number of users to make all requests simultaneously
        ForkJoinPool forkJoinPool = new ForkJoinPool(20);
        WorkThrottlerSemaphore workThrottlerSemaphore = new WorkThrottlerSemaphore(allQueriesLimit, allUsersLimit, workPerUserLimit,  workTimeInMillis); // MyClass is tested

        List<String> combinedNames = new ArrayList<>(sameNames);
        combinedNames.addAll(differentNames);
        // run all requests and get results of execution to boolean list
        List<Boolean> resultList = forkJoinPool.submit(
                () -> combinedNames.parallelStream().map(s -> {
                    boolean result = workThrottlerSemaphore.work(s);
                    System.out.println(result);
                    return result;
                }).collect(Collectors.toList())).get();

        assertEquals(20, resultList.size());

        // submitted successfully 11 works
        assertEquals(11, (int) resultList.stream().filter(aBoolean -> aBoolean).count());
        // 9 works not submitted
        assertEquals(9, (int) resultList.stream().filter(aBoolean -> !aBoolean).count());
        System.out.println(resultList);
    }
}
